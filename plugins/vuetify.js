import Vue from 'vue'
import Vuetify from 'vuetify'
// import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: '#607d8b',
    accent: '#848482',
    secondary: '#BA9B77',
    info: '#123266',
    warning: '#3d3635',
    error: '#B22222',
    success: '#75816b'
  }
})
